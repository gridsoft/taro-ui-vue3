import initWarnMatchers from './matchers'
import initMocks from './funcMocks'

initWarnMatchers()
initMocks()